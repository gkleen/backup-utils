# SPDX-FileCopyrightText: 2023-2024 Gregor Kleen
#
# SPDX-License-Identifier: GPL-3.0-or-later
{
  poetry2nix,
  python3Packages,
  zfs,
  util-linux,
  borgbackup,
  makeWrapper,
  lib,
  overrideWrapperArgs ? (wrapperArgs: wrapperArgs),
}:
with poetry2nix;
  mkPoetryApplication {
    projectDir = cleanPythonSources {src = ./.;};

    overrides = overrides.withDefaults (self: super: {
      pyprctl = super.pyprctl.overridePythonAttrs (oldAttrs: {
        buildInputs = (oldAttrs.buildInputs or []) ++ [super.setuptools];
      });
      str2bool = super.str2bool.overridePythonAttrs (oldAttrs: {
        buildInputs = (oldAttrs.buildInputs or []) ++ [super.setuptools];
      });

      inherit (python3Packages) python-unshare;
    });

    nativeBuildInputs = [makeWrapper];

    postInstall = ''
      wrapProgram $out/bin/borgsnap \
        ${lib.escapeShellArgs (overrideWrapperArgs [
        "--prefix"
        "PATH"
        ":"
        (lib.makeBinPath [zfs util-linux borgbackup])
      ])}
    '';
  }
