# SPDX-FileCopyrightText: 2023-2024 Gregor Kleen
#
# SPDX-License-Identifier: GPL-3.0-or-later
{
  inputs = {
    nixpkgs = {
      type = "github";
      owner = "NixOS";
      repo = "nixpkgs";
      ref = "24.05";
    };

    flake-parts = {
      type = "github";
      owner = "hercules-ci";
      repo = "flake-parts";
    };

    pre-commit-hooks-nix = {
      type = "github";
      owner = "cachix";
      repo = "pre-commit-hooks.nix";
    };

    poetry2nix = {
      type = "github";
      owner = "nix-community";
      repo = "poetry2nix";
      ref = "master";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };
  };

  outputs = inputs @ {
    self,
    nixpkgs,
    flake-parts,
    pre-commit-hooks-nix,
    poetry2nix,
    ...
  }:
    flake-parts.lib.mkFlake
    {inherit inputs;}
    ({...}: {
      imports = [
        flake-parts.flakeModules.easyOverlay
        pre-commit-hooks-nix.flakeModule
      ];

      config = {
        systems = ["x86_64-linux"];

        perSystem = {
          system,
          config,
          pkgs,
          ...
        }: {
          _module.args.pkgs = import inputs.nixpkgs {
            inherit system;
            overlays = [
              poetry2nix.overlays.default
            ];
            config = {};
          };

          devShells.default = pkgs.mkShell {
            buildInputs = with pkgs; [poetry reuse];
            shellHook = ''
              ${config.pre-commit.installationScript}
            '';
          };

          pre-commit = {
            settings.hooks = {
              reuse = {
                enable = true;
                pass_filenames = false;
                entry = "${config.pre-commit.pkgs.reuse}/bin/reuse lint";
              };
              black.enable = true;
              alejandra.enable = true;
            };
          };

          packages = {
            borgsnap = pkgs.callPackage ./borgsnap {};
            zfssnap = pkgs.callPackage ./zfssnap {};
          };

          overlayAttrs = {
            inherit (config.packages) borgsnap zfssnap;
          };
        };

        flake.nixosModules = {
          borgsnap = import ./borgsnap.nix;
          zfssnap = import ./zfssnap.nix;

          default = {config, ...}: {
            imports = with self.nixosModules; [borgsnap zfssnap];

            config.nixpkgs.overlays = [self.overlays.default];
          };
        };
      };
    });
}
