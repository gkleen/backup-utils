# SPDX-FileCopyrightText: 2023 Gregor Kleen
#
# SPDX-License-Identifier: GPL-3.0-or-later
{
  config,
  pkgs,
  lib,
  ...
}:
with lib; let
  cfg = config.services.zfssnap;

  configFormat = pkgs.formats.ini {
    mkSectionName = name: strings.escape ["[" "]"] (strings.toUpper name);
  };
in {
  options = {
    services.zfssnap = {
      enable = mkEnableOption "zfssnap service";

      package = mkOption {
        type = types.package;
        default = pkgs.zfssnap.override {
          zfs = config.boot.zfs.package;
        };
        defaultText = ''
          pkgs.zfssnap.override {
            zfs = config.boot.zfs.package;
          }
        '';
        description = "Package to use";
      };

      config = mkOption {
        type = types.submodule {
          freeformType = configFormat.type;

          options = {
            keep = mkOption {
              type = with types; attrsOf str;
              default = {
                within = "15m";
                "5m" = "48";
                "15m" = "32";
                hourly = "48";
                "4h" = "24";
                "12h" = "12";
                daily = "62";
                halfweekly = "32";
                weekly = "24";
                monthly = "-1";
              };
            };
            exec = mkOption {
              type = with types; attrsOf str;
              default = {};
            };
          };
        };
        description = "Configuration";
      };

      snapInterval = mkOption {
        type = types.str;
        default = "*-*-* *:00/5:00";
        description = "Systemd timer interval";
      };

      verbosity = mkOption {
        type = types.int;
        default = 2;
        description = "Verbosity";
      };

      extraPruneArgs = mkOption {
        type = with types; listOf str;
        default = [];
        description = "Extra arguments to pass to prune subcommand";
      };
      extraAutosnapArgs = mkOption {
        type = with types; listOf str;
        default = [];
        description = "Extra arguments to pass to autosnap subcommand";
      };
    };
  };

  config = mkIf cfg.enable {
    systemd.services."zfssnap" = {
      description = "Create automatic ZFS snapshots";
      after = ["zfs-import.target"];
      wants = ["zfssnap-prune.service"];
      before = ["zfssnap-prune.service"];
      serviceConfig = {
        Type = "oneshot";
        ExecStart = "${cfg.package}/bin/zfssnap --verbosity=${toString cfg.verbosity} autosnap ${escapeShellArgs cfg.extraAutosnapArgs}";

        LogRateLimitIntervalSec = 0;
      };
    };
    systemd.services."zfssnap-prune" = {
      description = "Prune automatic ZFS snapshots";
      after = ["zfs-import.target" "zfssnap.service"];
      serviceConfig = {
        Type = "oneshot";
        ExecStart = "${cfg.package}/bin/zfssnap --verbosity=${toString cfg.verbosity} prune --config=${configFormat.generate "zfssnap.ini" cfg.config} ${escapeShellArgs cfg.extraPruneArgs}";

        LogRateLimitIntervalSec = 0;
      };
    };

    systemd.timers."zfssnap" = {
      wantedBy = ["timers.target"];
      timerConfig = {
        OnCalendar = cfg.snapInterval;
        Persistent = true;
      };
    };
  };
}
