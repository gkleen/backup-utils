# SPDX-FileCopyrightText: 2023-2024 Gregor Kleen
#
# SPDX-License-Identifier: GPL-3.0-or-later
{
  poetry2nix,
  zfs,
  makeWrapper,
  lib,
  overrideWrapperArgs ? (wrapperArgs: wrapperArgs),
}:
with poetry2nix;
  mkPoetryApplication {
    projectDir = cleanPythonSources {src = ./.;};

    nativeBuildInputs = [makeWrapper];

    overrides =
      poetry2nix.defaultPoetryOverrides.extend
      (final: prev: {
        str2bool =
          prev.str2bool.overridePythonAttrs
          (
            old: {
              buildInputs = (old.buildInputs or []) ++ [prev.setuptools];
            }
          );
      });

    postInstall = ''
      wrapProgram $out/bin/zfssnap \
        ${lib.escapeShellArgs (overrideWrapperArgs [
        "--prefix"
        "PATH"
        ":"
        (lib.makeBinPath [zfs])
      ])}
    '';
  }
