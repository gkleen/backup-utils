# SPDX-FileCopyrightText: 2023 Gregor Kleen
#
# SPDX-License-Identifier: GPL-3.0-or-later
{
  config,
  pkgs,
  lib,
  flakeInputs,
  hostName,
  ...
}:
with lib; let
  cfg = config.services.borgsnap;
in {
  options = {
    services.borgsnap = {
      enable = mkEnableOption "borgsnap service";

      package = mkOption {
        type = types.package;
        default = pkgs.borgsnap.override {
          zfs = config.boot.zfs.package;
          overrideWrapperArgs = oldArgs:
            oldArgs
            ++ [
              "--prefix"
              "PATH"
              ":"
              config.security.wrapperDir
            ];
        };
        defaultText = literalExpression ''
          pkgs.borgsnap.override {
            zfs = config.boot.zfs.package;
            overrideWrapperArgs = oldArgs: oldArgs ++ [
              "--prefix" "PATH" ":" config.security.wrapperDir
            ];
          }
        '';
        description = "Package to use";
      };

      target = mkOption {
        type = types.str;
        description = "Target borg repository";
      };

      archive-prefix = mkOption {
        type = types.str;
        default = "${config.networking.hostName}.";
        defaultText = literalExpression ''"''${config.networking.hostName}."'';
        description = "Prefix to prepend to borg archives";
      };

      extraConfig = mkOption {
        type = with types; attrsOf str;
        default = {
          halfweekly = "8";
          monthly = "-1";
        };
        description = "Additional configuration for zfssnap";
      };

      verbosity = mkOption {
        type = types.int;
        default = config.services.zfssnap.verbosity;
        defaultText = literalExpression "config.services.zfssnap.verbosity";
        description = "Set Verbosity";
      };

      sshConfig = mkOption {
        type = with types; nullOr str;
        default = null;
        description = "SSH client config to pass to borg";
      };

      keyfile = mkOption {
        type = with types; nullOr str;
        default = null;
        description = "Keyfile to pass to borg";
      };

      extraCreateArgs = mkOption {
        type = with types; listOf str;
        default = [];
        description = "Extra arguments to pass to create subcommand";
      };
      extraCheckArgs = mkOption {
        type = with types; listOf str;
        default = [];
        description = "Extra arguments to pass to check subcommand";
      };

      unknownUnencryptedRepoAccessOk = mkOption {
        type = types.bool;
        default = false;
        description = "Set `BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK`?";
      };
      hostnameIsUnique = mkOption {
        type = types.bool;
        default = true;
        description = "Set `BORG_HOSTNAME_IS_UNIQUE`?";
      };
      borgBaseDir = mkOption {
        type = types.nullOr types.str;
        default = "/var/lib/borg";
        description = "Where to store borg state";
      };
    };
  };

  config = mkIf cfg.enable {
    warnings = mkIf (!config.services.zfssnap.enable) [
      "borgsnap will do nothing if zfssnap is not enabled"
    ];

    services.zfssnap.config.exec =
      {
        check = "${cfg.package}/bin/borgsnap --verbosity=${toString cfg.verbosity} --target ${escapeShellArg cfg.target} --archive-prefix ${escapeShellArg cfg.archive-prefix} check --cache-file /run/zfssnap-prune/archives-cache.json ${escapeShellArgs cfg.extraCheckArgs}";
        cmd = "${cfg.package}/bin/borgsnap --verbosity=${toString cfg.verbosity} --target ${escapeShellArg cfg.target} --archive-prefix ${escapeShellArg cfg.archive-prefix} create ${escapeShellArgs cfg.extraCreateArgs}";
      }
      // cfg.extraConfig;

    systemd.services."zfssnap-prune" = {
      serviceConfig = {
        Environment =
          optionals (!(isNull cfg.borgBaseDir)) [
            "BORG_BASE_DIR=${cfg.borgBaseDir}"
            "BORG_CONFIG_DIR=${cfg.borgBaseDir}/config"
            "BORG_CACHE_DIR=${cfg.borgBaseDir}/cache"
            "BORG_SECURITY_DIR=${cfg.borgBaseDir}/security"
            "BORG_KEYS_DIR=${cfg.borgBaseDir}/keys"
          ]
          ++ optional cfg.unknownUnencryptedRepoAccessOk "BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK=yes"
          ++ optional cfg.hostnameIsUnique "BORG_HOSTNAME_IS_UNIQUE=yes"
          ++ optional (!(isNull cfg.sshConfig)) "BORG_RSH=\"${pkgs.openssh}/bin/ssh -F ${pkgs.writeText "config" cfg.sshConfig}\""
          ++ optional (!(isNull cfg.keyfile)) "BORG_KEY_FILE=${cfg.keyfile}";
        RuntimeDirectory = "zfssnap-prune";
      };
    };
  };
}
